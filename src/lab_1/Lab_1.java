package lab_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Вера
 */
public class Lab_1 {
    
    public List<Student> list;
public class Student
  {
      private final String Surname ;
      private final String Group;
      
      Student (String surname, String group)
      {
        Surname=surname;
        Group=group;
      }
      public String getSurname()
      {
          return Surname;
      }
      
      public String getGroup ()
      {
          return Group;
      }
      public String toString ()
      {
      return Surname;
      }
  }
  
  public void initialize()
  {
      list=new ArrayList<Student>();
      list.add(new Student ("Иванов", "ИДБ-12-04"));
      list.add(new Student ("Медведев", "ИДБ-12-02"));
      list.add(new Student ("Каблова", "ИДБ-12-02"));
      list.add(new Student ("Малахина", "ИДБ-12-11"));
      list.add(new Student ("Сидоров", "ИДБ-12-02"));
      list.add(new Student ("Скоркина", "ИДБ-12-04"));
      list.add(new Student ("Белов", "ИДБ-12-11"));
      list.add(new Student ("Антонов", "ИДБ-12-12"));
  }
  
  public void print()
  {
      for(Student student: list)
      {
          System.out.println(student.getSurname() + " " + student.getGroup());
      }
  }
  
 
  public void groupByGroup()
  {
      Map<String, List<Student> > map= list
              .stream()
              .sorted((arg1,arg2) -> arg1.getSurname().compareTo(arg2.getSurname()))
              .collect(Collectors.groupingBy(Student::getGroup));
    
                      
      System.out.println(map);
  }
    public static void main(String[] args) {
        Lab_1 lab = new Lab_1();
        lab.initialize();
        lab.groupByGroup();
       }
    
}
